import sys
from image_processor import ImageProcessor

#
# Usage:
# python3 preprocess.py [patient_folder]
# if patient_folder is provided, process only that patient
# if patient_folder is not provided, will process all patients under DICOM_FOLDER
#

ip = ImageProcessor('../images/sample_images', '../images/sample_out')

if len(sys.argv) == 1:
    ip.process_all_patients()
else:
    patient_folder = sys.argv[1]
    ip.substract_patient(patient_folder)

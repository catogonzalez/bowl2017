import sys
from image_processor import ImageProcessor
import numpy as np
#
# Usage:
# python3 preprocess.py [patient_folder]
# if patient_folder is provided, process only that patient
# if patient_folder is not provided, will process all patients under DICOM_FOLDER
#

ip = ImageProcessor('../images/sample_images', '../images/sample_work')

if len(sys.argv) == 1:
    ip.process_all_patients()
else:
    patient_folder = sys.argv[1]
    ip.process_patient(patient_folder, True)

slices = ip.load_patient('ca-0015ceb851d7251b8f399e39779d1e7d')

mid_slice = round(len(slices) / 2)

# Convert to HU and resample to 1x1x1
# pxls = [x.pixel_array for x in slices]
# Convert to HU
pxls = ip.get_pixels_hu(slices)
# resample to 1x1x1
pix_resampled, spacing = ip.resample(pxls, slices, [1, 1, 1])
# pix_normalized = ip.normalize(pix_resampled)
# diff = ip.pixels_diff(pix_resampled, 2)
print("Shape before resampling\t", pxls.shape)
print("Shape after resampling\t", pix_resampled.shape)

# chest segmentation - calc thresholds

# from all slices, get diagonal histogram to determine thresholds
all_diagonals = [p.diagonal() for p in pix_resampled]
master_diagonal = np.concatenate(all_diagonals).ravel()

# calc thresholds
hist = np.histogram(master_diagonal.flatten(), bins='auto')
# get indices of two peaks
indices=hist[0].argsort()[-2:]
p1 = hist[1][indices[0]]
p2 = hist[1][indices[1]]
if p2<p1:
    # swap the values
    p1,p2 = p2,p1
L=(p1+p2)/2.
print('Histogram of diagonals peaks at {0} and {1} so threshold is HU = {2}'.format(p1,p2,L))


# chest segmentation - apply region growing mask
mask, segmented_chest = ip.apply_region_growing(pix_resampled[90], [0,0], L)



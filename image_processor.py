import glob
import os
import time
import uuid

import dicom, dicom.UID
from dicom.dataset import Dataset, FileDataset

import numpy as np
import scipy.ndimage
from skimage import measure
from skimage.segmentation import random_walker

import pandas as pd

# based on tutorial - used for lung segmentation
from skimage import morphology
from skimage import measure
from sklearn.cluster import KMeans
from skimage.transform import resize


class ImageProcessor:
    """
        DICOM Image processing library. Specially created for https://www.kaggle.com/c/data-science-bowl-2017
        dicom_folder: path to .dcm folders each containing set of patient's dcm files
        The system will create work_folder (if not exists) and also create this structure under work_folder:
            images: to hold .npy numpy files for each patient with shape [num_images, x, y]
            masks: to hold .npy numpy files for each images with shape [num_masks, x, y]


    """

    def __init__(self, dicom_folder, work_folder):
        # Make sure paths exist
        if not os.path.exists(dicom_folder):
            raise IOError("Folder %s does not exist." % dicom_folder)

        # ensure output target_folder exists
        if not os.path.exists(work_folder):
            os.makedirs(work_folder)

        self.dicom_folder = dicom_folder
        self.work_folder = work_folder

        # ensure npy folder exists
        self.images_folder = os.path.join(self.work_folder, 'images')
        if not os.path.exists(self.images_folder):
            os.makedirs(self.images_folder)

        self.voxel_spacings_csv = os.path.join(self.images_folder, 'voxel_spacings.csv')

        # ensure masks folder exists
        self.masks_folder = os.path.join(self.work_folder, 'masks')
        if not os.path.exists(self.masks_folder):
            os.makedirs(self.masks_folder)

        # ensure segmented folder exists
        self.segmented_folder = os.path.join(self.work_folder, 'segmented')
        if not os.path.exists(self.segmented_folder):
            os.makedirs(self.segmented_folder)

    def load_scan(self, folder):
        # Load the scans in given folder
        slices = [dicom.read_file(s) for s in glob.glob(os.path.join(folder, '*.dcm'))]
        slices.sort(key=lambda x: int(x.ImagePositionPatient[2]))
        try:
            slice_thickness = np.abs(slices[0].ImagePositionPatient[2] - slices[1].ImagePositionPatient[2])
        except:
            slice_thickness = np.abs(slices[0].SliceLocation - slices[1].SliceLocation)

        for s in slices:
            s.SliceThickness = slice_thickness

        return slices

    def get_pixels_hu(self, slices):
        # convert all values to Hounsfield Unit (HU)
        # U = m*SV + b
        # where U is in output units, m is the rescale slope, SV is the stored value, and b is the rescale intercept
        images = np.stack([s.pixel_array for s in slices])
        # Convert to int16 (from sometimes int16),
        # should be possible as values should always be low enough (<32k)
        images = images.astype(np.int16)

        # Set outside-of-scan pixels to 0
        # The intercept is usually -1024, so air is approximately 0
        images[images == -2000] = 0

        # Convert to Hounsfield units (HU)
        for slice_number in range(len(slices)):

            intercept = slices[slice_number].RescaleIntercept
            slope = slices[slice_number].RescaleSlope

            if slope != 1:
                images[slice_number] = slope * images[slice_number].astype(np.float64)
                images[slice_number] = images[slice_number].astype(np.int16)

            images[slice_number] += np.int16(intercept)

        return np.array(images, dtype=np.int16)

    def resample(self, images, scan, new_spacing=[1, 1, 1]):
        # resampling the full dataset to a 1mm x 1mm x 1mm isotropic resolution

        # Determine current pixel spacing
        spacing = np.array([scan[0].SliceThickness] + scan[0].PixelSpacing, dtype=np.float32)

        resize_factor = spacing / new_spacing
        new_real_shape = images.shape * resize_factor
        new_shape = np.round(new_real_shape)
        real_resize_factor = new_shape / images.shape
        new_spacing = spacing / real_resize_factor

        images = scipy.ndimage.interpolation.zoom(images, real_resize_factor, mode='nearest')

        return images, new_spacing

    def save_images_as_npy(self, images, spacing, patient_id):
        # save a whole scan (multiple images) to npy file
        outfile = os.path.join(self.images_folder, '%s.npy' % patient_id)
        np.save(outfile, images)

        # save voxel spacing to 'voxel_spacings.csv'
        cols = ['spacing_x', 'spacing_y', 'spacing_z', ]
        d = {'spacing_x': spacing[1], 'spacing_y': spacing[2], 'spacing_z': spacing[0]}  # nte the indices
        df = pd.DataFrame(data=d, columns=cols, index=[patient_id])

        if not os.path.exists(self.voxel_spacings_csv):
            df.to_csv(self.voxel_spacings_csv, index_label='patient_id')
        else:
            df_on_disk = pd.read_csv('voxel_spacings.csv', index_col='patient_id')
            df_on_disk.index.name = 'patient_id'
            df.index.name = 'patient_id'
            df_on_disk.update(df)
            df_on_disk.to_csv('voxel_spacings.csv', index_label='patient_id')

    def create_lungs_mask(self, img):
        # create mask for one image

        # save original image
        original = img

        # Standardize the pixel values
        mean = np.mean(img)
        std = np.std(img)
        img = img - mean
        img = img / std

        # get image center as percentage of the total area
        margin_percent = 0.10
        x_margin = round(img.shape[0] * margin_percent)
        y_margin = round(img.shape[1] * margin_percent)
        x_max = img.shape[0] - x_margin
        y_max = img.shape[1] - y_margin
        diag_max = y_max - x_margin

        # Find the average pixel value near the lungs
        # to renormalize washed out images
        middle = img[x_margin:x_max, y_margin:y_max]
        mean = np.mean(middle)
        max = np.max(img)
        min = np.min(img)

        # To improve threshold finding, I'm moving the
        # underflow and overflow on the pixel spectrum
        img[img == max] = mean
        img[img == min] = mean

        # Using Kmeans to separate foreground (radio-opaque tissue)
        # and background (radio transparent tissue ie lungs)
        # Doing this only on the center of the image to avoid
        # the non-tissue parts of the image as much as possible
        kmeans = KMeans(n_clusters=2).fit(np.reshape(middle, [np.prod(middle.shape), 1]))
        centers = sorted(kmeans.cluster_centers_.flatten())
        threshold = np.mean(centers)
        thresh_img = np.where(img < threshold, 1.0, 0.0)  # threshold the image

        # I found an initial erosion helful for removing graininess from some of the regions
        # and then large dialation is used to make the lung region
        # engulf the vessels and incursions into the lung cavity by
        # radio opaque tissue
        eroded = morphology.erosion(thresh_img, np.ones([4, 4]))
        dilation = morphology.dilation(eroded, np.ones([10, 10]))

        #  Label each region and obtain the region properties
        #  The background region is removed by removing regions
        #  with a bbox that is to large in either dimnsion
        #  Also, the lungs are generally far away from the top
        #  and bottom of the image, so any regions that are too
        #  close to the top and bottom are removed
        #  This does not produce a perfect segmentation of the lungs
        #  from the image, but it is surprisingly good considering its
        #  simplicity.
        labels = measure.label(dilation)
        label_vals = np.unique(labels)
        regions = measure.regionprops(labels)
        good_labels = []
        for prop in regions:
            B = prop.bbox
            # Bounding box (min_row, min_col, max_row, max_col)
            if B[2] - B[0] < diag_max and B[3] - B[1] < diag_max and B[0] > x_margin and B[2] < y_max:
                good_labels.append(prop.label)
        mask = np.ndarray(img.shape, dtype=np.int16)
        mask[:] = 0

        #  The mask here is the mask for the lungs--not the nodes
        #  After just the lungs are left, we do another large dilation
        #  in order to fill in and out the lung mask
        for N in good_labels:
            mask = mask + np.where(labels == N, 1, 0)

        # one last dilation
        mask = morphology.dilation(mask, np.ones([10, 10]))

        # calcs segmented original image
        segmented = mask * original
        # set outer area of interest to air HU (-1000)
        segmented[mask == 0] = -1000

        # now create a new mask to include just the parenchyma (i.e. HU values < -400)
        msk = np.where(segmented < -400, 1.0, 0.0)
        seg = segmented * msk
        seg[seg == 0] = -1000

        # #
        # # renormalizing the masked image (in the mask region)
        # #
        # new_mean = np.mean(segmented[mask > 0])
        # new_std = np.std(segmented[mask > 0])
        # #
        # #  Pulling the background color up to the lower end
        # #  of the pixel range for the lungs
        # #
        # old_min = np.min(segmented)  # background color
        # segmented[segmented == old_min] = new_mean - 1.2 * new_std  # resetting backgound color
        # segmented = segmented - new_mean
        # segmented = segmented / new_std

        return msk, seg

    def create_lungs_masks(self, images, patient_id):
        # create mask for a whole set of images
        # images is of shape [num_images, x,y]

        masks = []
        segments = []
        for i in range(images.shape[0]):
            mask, segment = self.create_lungs_mask(images[i])
            masks.append(mask)
            segments.append(segment)

        masks = np.array(masks, dtype=np.int16)
        segments = np.array(segments, dtype=np.int16)

        # save a whole set of masks (multiple images) to npy file
        outfile = os.path.join(self.masks_folder, '%s.npy' % patient_id)
        np.save(outfile, masks)

        # save a whole set of segmented images to npy file
        outfile = os.path.join(self.segmented_folder, '%s.npy' % patient_id)
        np.save(outfile, segments)

        return masks, segments

    def segment_lung_mask(self, image, fill_lung_structures=True):
        # used for Area of Interest (AOI = lung) segmentation
        def largest_label_volume(im, bg=-1):
            vals, counts = np.unique(im, return_counts=True)

            counts = counts[vals != bg]
            vals = vals[vals != bg]

            if len(counts) > 0:
                return vals[np.argmax(counts)]
            else:
                return None

        # not actually binary, but 1 and 2.
        # 0 is treated as background, which we do not want
        binary_image = np.array(image > -320, dtype=np.int8) + 1
        labels = measure.label(binary_image)

        # Pick the pixel in the very corner to determine which label is air.
        #   Improvement: Pick multiple background labels from around the patient
        #   More resistant to "trays" on which the patient lays cutting the air
        #   around the person in half
        background_label = labels[0, 0, 0]

        # Fill the air around the person
        binary_image[background_label == labels] = 2

        # Method of filling the lung structures (that is superior to something like
        # morphological closing)
        if fill_lung_structures:
            # For every slice we determine the largest solid structure
            for i, axial_slice in enumerate(binary_image):
                axial_slice = axial_slice - 1
                labeling = measure.label(axial_slice)
                l_max = largest_label_volume(labeling, bg=0)

                if l_max is not None:  # This slice contains some lung
                    binary_image[i][labeling != l_max] = 1

        binary_image -= 1  # Make the image actual binary
        binary_image = 1 - binary_image  # Invert it, lungs are now 1

        # Remove other air pockets insided body
        labels = measure.label(binary_image, background=0)
        l_max = largest_label_volume(labels, bg=0)
        if l_max is not None:  # There are air pockets
            binary_image[labels != l_max] = 0

        return binary_image

    def normalize(self, image):
        MIN_BOUND = -1000.0
        MAX_BOUND = 400.0
        image = (image - MIN_BOUND) / (MAX_BOUND - MIN_BOUND)
        image[image > 1] = 1.
        image[image < 0] = 0.
        return image

    def pixels_to_dicom(self, original_dicom, index, pixel_array, target_patient_folder, suffix=''):
        """ Builds a new dicom file mixing metadata from the original_dicom replacing pixel_array
            into the new dicom file; the new dicom file is saved in he target_patient_folder with a random UUID name
         """
        filename = str(uuid.uuid4()).replace('-', '') + '.dcm'
        filename = os.path.join(target_patient_folder, filename)

        ds = FileDataset(filename, {}, file_meta=original_dicom.file_meta)
        ds.Modality = 'OT'
        ds.PatientID = original_dicom.PatientID
        ds.PatientsName = str(original_dicom.PatientsName) + suffix
        ds.SeriesNumber = index

        ds.StudyInstanceUID = original_dicom.StudyInstanceUID
        ds.SeriesInstanceUID = original_dicom.SeriesInstanceUID
        ds.SOPInstanceUID = original_dicom.SOPInstanceUID
        ds.SOPClassUID = original_dicom.SOPClassUID
        ds.SeriesDescription = 'Axial'
        ds.PhotometricInterpretation = "MONOCHROME2"

        ds.SamplesPerPixel = 1
        ds.PixelRepresentation = 0
        ds.HighBit = 15
        ds.BitsStored = 16
        ds.BitsAllocated = 16

        ds.Columns = pixel_array.shape[0]
        ds.Rows = pixel_array.shape[1]
        if pixel_array.dtype != np.uint16:
            pixel_array = pixel_array.astype(np.uint16)
        ds.PixelData = pixel_array.tostring()

        ds.save_as(filename)
        return

    # load dicom files of  one patient in given dicom_folder
    def load_patient(self, patient_folder):
        source_folder = os.path.join(self.dicom_folder, patient_folder)
        if os.path.isdir(source_folder):
            # read dicom slices
            print('Processing %s' % source_folder)
            slices = self.load_scan(source_folder)
            print(' > %d slices read' % len(slices))

            return slices
        else:
            print('>>> Skipping %s: not a folder' % source_folder)
            return None

    # process one patient in given dicom_folder
    def process_patient(self, patient_folder, create_pixel_dicom=False):
        source_folder = os.path.join(self.dicom_folder, patient_folder)
        if os.path.isdir(source_folder):
            # measure execution time
            start = time.time()

            # read dicom slices
            print('Processing %s' % source_folder)
            slices = self.load_scan(source_folder)
            print(' > %d slices read' % len(slices))

            print(' > Converting to HU...')
            pixels = self.get_pixels_hu(slices)

            # resample to 1x1x1
            print(' > Resampling')
            pix_resampled, spacing = self.resample(pixels, slices, [1, 1, 1])
            print("  > Shape before resampling\t", pixels.shape)
            print("  > Shape after resampling\t", pix_resampled.shape)

            # save to .npy
            self.save_npy(pix_resampled, spacing, patient_folder)
            outfile = os.path.join(self.images_folder, '%s.npy' % patient_folder)
            print(' > %s saved' % outfile)

            # create lungs mask
            print(' > Creating lungs mask')
            # segmented_lungs = self.segment_lung_mask(pix_resampled, False)
            lungs_mask = self.create_lung_mask(pix_resampled)

            # outfile = os.path.join(self.work_folder, '%s.npy' % patient_folder)
            # np.save(outfile, segmented_lungs)
            # print(' > %s saved' % outfile)

            # if create_pixel_dicom:
            #     print(' > Creating {0} new dicoms in {1}'.format(segmented_lungs.shape[0], patient_folder))
            #     target_patient_folder = os.path.join(self.work_folder, patient_folder)
            #
            #     # ensure target_patient_folder exists
            #     if not os.path.exists(target_patient_folder):
            #         os.makedirs(target_patient_folder)
            #
            #     for i in range(len(segmented_lungs)):
            #         self.pixels_to_dicom(slices[0], i, segmented_lungs[i], target_patient_folder)
            #
            #     end = time.time()
            #     print('%s seconds to process.' % time.strftime('%H:%M:%S', time.gmtime(end - start)))

        else:
            print('>>> Skipping %s: not a folder' % source_folder)
            return None

    # process all the patients in dicom_folder and save .npy in npy_folder
    def process_all_patients(self):
        patient_folders = os.listdir(self.dicom_folder)
        for patient_folder in patient_folders:
            self.process_patient(patient_folder)  #

            # PIXEL_MEAN = 0.25
            #
            #
            # def zero_center(image):
            #     image = image - PIXEL_MEAN
            #     return image
